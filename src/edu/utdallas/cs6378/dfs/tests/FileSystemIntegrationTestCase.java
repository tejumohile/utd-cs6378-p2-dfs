package edu.utdallas.cs6378.dfs.tests;

import edu.utdallas.cs6378.dfs.errors.InvalidModeException;
import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.errors.ReadFailedException;
import edu.utdallas.cs6378.dfs.errors.WriteFailedException;
import edu.utdallas.cs6378.dfs.filesystem.FileHandle;
import edu.utdallas.cs6378.dfs.filesystem.FileMode;
import edu.utdallas.cs6378.dfs.filesystem.FileSystem;
import edu.utdallas.cs6378.dfs.filesystem.Peer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mark on 8/2/14.
 */
public class FileSystemIntegrationTestCase {
    private Peer p1;
    private Peer p2;
    private Peer p3;
    private Peer p4;

    private FileSystem fs1;
    private FileSystem fs2;
    private FileSystem fs3;
    private FileSystem fs4;
    private int r;
    private int w;

    @Before
    public void setup() throws IOException {
        p1 = new Peer(new InetSocketAddress("127.0.0.1", 5454), 4);
        p2 = new Peer(new InetSocketAddress("127.0.0.1", 5455), 4);
        p3 = new Peer(new InetSocketAddress("127.0.0.1", 5456), 4);
        p4 = new Peer(new InetSocketAddress("127.0.0.1", 5457), 4);

        r = 8;
        w = 9;

        fs1 = FileSystem.join(4, p1.getAddress(), 5454, Arrays.asList(new Peer[]{p2, p3, p4}), r, w);
        fs2 = FileSystem.join(4, p2.getAddress(), 5455, Arrays.asList(new Peer[]{p1, p3, p4}), r, w);
    }

    @After
    public void teardown() throws IOException, InterruptedException {
        if (fs1 != null && !fs1.isClosed()){
            fs1.close();
        }

        if (fs2 != null && !fs2.isClosed()){
            fs2.close();
        }

        if (fs3 != null && !fs3.isClosed()){
            fs3.close();
        }

        if (fs4 != null && !fs4.isClosed()){
            fs4.close();
        }

        Thread.sleep(200);
    }

    public void initFS3() throws IOException {
        fs3 = FileSystem.join(4, p3.getAddress(), 5456, Arrays.asList(new Peer[]{p1, p2, p4}), r, w);
    }

    public void initFS4() throws IOException {
        fs4 = FileSystem.join(4, p4.getAddress(), 5457, Arrays.asList(new Peer[]{p1, p2, p3}), r, w);
    }

    @Test
    public void testTwoOpenFiles() throws LockAcquisitionException, ReadFailedException, InvalidModeException, IOException {
        FileHandle handle = fs1.openFile("testfile.txt", FileMode.READ);

        initFS3(); initFS4();

        FileHandle handle2 = fs3.openFile("testfile.txt", FileMode.READ);
    }

    @Test
    public void testTwoReadFiles() throws LockAcquisitionException, ReadFailedException, InvalidModeException, IOException {
        FileHandle handle = fs1.openFile("testfile.txt", FileMode.READ);

        initFS3(); initFS4();

        FileHandle handle2 = fs3.openFile("testfile.txt", FileMode.READ);
        String content = handle.read();
        String content2 = handle2.read();

        assertEquals(content, content2);
    }

    @Test
    public void testWriteThenReadFromDifferent() throws LockAcquisitionException, ReadFailedException, InvalidModeException, IOException, WriteFailedException {
        String expected = "Hello World!";

        initFS3(); initFS4();

        FileHandle handle = fs1.openFile("testfile.txt", FileMode.WRITE);
        handle.write(expected);
        handle.close();

        handle = fs2.openFile("testfile.txt", FileMode.READ);
        String content = handle.read();
        handle.close();

        assertEquals(expected, content);
    }

    @Test
    public void testReadThenWriteFromDeadNode() throws LockAcquisitionException, ReadFailedException, InvalidModeException, IOException, WriteFailedException {
        String expected = "Hello World!";

        initFS3();

        FileHandle handle = fs1.openFile("testfile.txt", FileMode.WRITE);
        handle.write(expected);
        handle.close();

        initFS4();

        handle = fs4.openFile("testfile.txt", FileMode.READ);
        String content = handle.read();
        handle.close();

        assertEquals(expected, content);
    }
}

package edu.utdallas.cs6378.dfs.logging;

import edu.utdallas.cs6378.dfs.filesystem.FileMode;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.*;

/**
 * Created by mark on 8/3/14.
 */
public interface TxLog {
    public void write(String filename, FileMode op, int version, String content);
    public void close() throws IOException;
}

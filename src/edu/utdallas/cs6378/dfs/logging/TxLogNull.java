package edu.utdallas.cs6378.dfs.logging;

import edu.utdallas.cs6378.dfs.filesystem.FileMode;

import java.io.IOException;

/**
 * Created by mark on 8/3/14.
 */
public class TxLogNull implements TxLog {

    @Override
    public void write(String filename, FileMode op, int version, String content) {

    }

    @Override
    public void close() throws IOException {

    }
}

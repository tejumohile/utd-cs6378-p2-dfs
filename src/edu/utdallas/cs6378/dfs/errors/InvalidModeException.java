package edu.utdallas.cs6378.dfs.errors;

/**
 * Created by mark on 7/22/14.
 */
public class InvalidModeException extends Exception {
    public InvalidModeException(){
        super("The file is not currently in a valid mode for this operation");
    }

}

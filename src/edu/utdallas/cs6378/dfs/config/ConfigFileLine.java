package edu.utdallas.cs6378.dfs.config;

import edu.utdallas.cs6378.dfs.filesystem.FileMode;

/**
 * Created by mark on 8/2/14.
 */
public class ConfigFileLine {
    private FileMode operation;
    private String filename;
    private String content;

    public ConfigFileLine(String line){
        String[] parts = line.split("\t", 3);

        switch(parts[0]){
            case "R":
                operation = FileMode.READ;
                break;
            case "W":
                operation = FileMode.WRITE;
                break;
            default:
                throw new IllegalArgumentException("Invalid operation. Must be 'R' or 'W'");
        }

        filename = parts[1];

        if (operation == FileMode.WRITE){
            content = parts[2];
        }
    }

    public FileMode getOperation(){
        return operation;
    }

    public String getFilename(){
        return filename;
    }

    public String getContent(){
        return content;
    }

    @Override
    public String toString(){
        return getOperation().toString() + "-" + getFilename() + "-" + getContent();
    }
}

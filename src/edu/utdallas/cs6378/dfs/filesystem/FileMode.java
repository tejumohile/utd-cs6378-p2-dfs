package edu.utdallas.cs6378.dfs.filesystem;

public enum FileMode {
    READ, WRITE
}

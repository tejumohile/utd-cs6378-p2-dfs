package edu.utdallas.cs6378.dfs.filesystem;

import edu.utdallas.cs6378.dfs.channels.Channel;
import edu.utdallas.cs6378.dfs.channels.TcpNetworkChannel;
import edu.utdallas.cs6378.dfs.errors.InvalidModeException;
import edu.utdallas.cs6378.dfs.errors.LockAcquisitionException;
import edu.utdallas.cs6378.dfs.errors.ReadFailedException;
import edu.utdallas.cs6378.dfs.errors.WriteFailedException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by mark on 7/22/14.
 */
public abstract class FileSystem{

    protected int localVotes;
    protected Channel channel;
    protected List<Peer> peers;
    protected int readQuorumSize;
    protected int writeQuorumSize;
    protected ConcurrentHashMap<String, LocalFile> files = new ConcurrentHashMap<>();

    public static FileSystem join(int myVotes, String hostAddress, int hostPort, List<Peer> peers, int readQuorumSize, int writeQuorumSize) throws IOException {
        return new FileSystemImpl(myVotes, hostAddress, new TcpNetworkChannel(hostPort), peers, readQuorumSize, writeQuorumSize, false);
    }

    public static FileSystem join(int myVotes, String hostAddress, int hostPort, List<Peer> peers, int readQuorumSize, int writeQuorumSize, boolean enableTxLog) throws IOException {
        return new FileSystemImpl(myVotes, hostAddress, new TcpNetworkChannel(hostPort), peers, readQuorumSize, writeQuorumSize, enableTxLog);
    }

    public static FileSystem join(int myVotes, String hostAddress, Channel channel, List<Peer> peers, int readQuorumSize, int writeQuorumSize) throws IOException {
        return new FileSystemImpl(myVotes, hostAddress, channel, peers, readQuorumSize, writeQuorumSize, false);
    }

    public FileSystem(int myVotes, Channel channel, List<Peer> peers, int readQuorumSize, int writeQuorumSize){
        this.localVotes = myVotes;
        this.channel = channel;
        this.peers = peers;

        int total_votes = myVotes;

        for (Peer p : peers){
            total_votes += p.getVotes();
        }

        if (writeQuorumSize <= (total_votes / 2)){
            throw new IllegalArgumentException("Invalid write quorum size: Must be greater than half the total number of votes");
        }

        if ((writeQuorumSize + readQuorumSize) <= total_votes){
            throw new IllegalArgumentException("Invalid parameters. The sum of read and write quorum sizes must be greater than the total number of votes.");
        }

        this.readQuorumSize = readQuorumSize;
        this.writeQuorumSize = writeQuorumSize;
    }

    public abstract FileHandle openFile(String filename, FileMode mode) throws LockAcquisitionException;
    public abstract String readFile(FileHandle handle) throws LockAcquisitionException, IOException, ReadFailedException, InvalidModeException;
    public abstract void writeFile(FileHandle handle, String content) throws InvalidModeException, WriteFailedException;
    public abstract void closeFile(FileHandle handle) throws LockAcquisitionException;
    public abstract void abort(FileHandle handle) throws IOException;
    public abstract void close() throws IOException;

    public abstract int getVersion(FileHandle handle);

    public abstract boolean isClosed();

}


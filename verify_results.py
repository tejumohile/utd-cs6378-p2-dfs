import argparse
from file import File

def parse_args():
    ''' Defines and parses the command line arguments for the application'''
    parser = argparse.ArgumentParser(description='Verifies results are consistent and valid')

    parser.add_argument('log_file', nargs='+', type=argparse.FileType('r'), help='The transaction logs to process (one per node)')
    parser.add_argument('cfg_file', type=argparse.FileType('r'), help='One of the configuration files for the nodes')

    return parser.parse_args()

''' Global dictionary for all the files and max versions for all files'''	
global files
''' files store the object of class File '''
files = {}
global files_max_version
''' Keeps track of the max version until some point in execution '''
files_max_version = {}
lines = []
all_nodes = []
''' Names of files written. Only distinct names'''
unique_files = []
'''Stores the replicas of the log file''' 
file_replicas = {}

def unique_file_version_pair(line_list):
    if not line_list[0] in files:        
        files[line_list[0]] = File(line_list[0])
        files[line_list[0]].addVersions(int(line_list[1]), line_list[2])
    file_hash = files[line_list[0]].versionExists(int(line_list[1]))
    if file_hash is not False:
        '''print "old hash =" + file_hash + " new =" + line_list[2]'''
        if line_list[2] != file_hash :
	    return False
    else:
    	files[line_list[0]].addVersions(int(line_list[1]), line_list[2])
    return True

def should_have_increasing_versions(line_list):
    
    if not line_list[0] in files_max_version: 
        files_max_version[line_list[0]] = int(line_list[1])
    else:
        '''print "old" + str(files_max_version[line_list[0]]) + "new" + line_list[1]'''
        if files_max_version[line_list[0]] <= int(line_list[1]):
            files_max_version[line_list[0]] = int(line_list[1]);
        else:
            '''print "Inside the false file max version"
            print files_max_version[line_list[0]]'''
            return False
    return True

def format_log(inf):
    results = []
    for line in inf:
        tup = line.strip().split('\t')        
        results.append((tup[0], int(tup[1]), tup[2]))
    return results

if __name__ == '__main__':
    args = parse_args()
    ''' Initializing read quorum, write quorum, weight for each process from cfg-file'''
    i = 0 
    peer_votes = {}
    for line in args.cfg_file:
        line_array = line.strip().split('\t')
	if i == 0 :
            peer_votes[line_array[0]] = int(line_array[1])
	if i == 1 :
            mode = 0;
            for pos in line_array:
                if mode == 0:
                    peer_votes[pos] = 0
                    pre_pos = pos
                    mode = 1
                elif mode == 1:
                    peer_votes[pre_pos] = pos
                    mode = 0
                
        if i == 2 :
            r = line_array[1]
            w = line_array[2]
        if i > 2 :
            break
        i = i + 1

    for txlog in args.log_file:
        files_max_version = {}
        file_result = txlog.readlines()
        all_nodes.extend(format_log(file_result))
	file_replicas[txlog] = file_result
        txlog.close()
    unique_entries = set(all_nodes)       

    for file in unique_files:
        max_version[file] = max([tup[1] for tup in all_nodes if tup[0] == file])

    for file in unique_files:
        file_entries = filter(lambda x: x[0] == file, unique_entries)
        unique_versions = set([entry[1] for entry in file_entries])
        if len(file_entries) != len(unique_versions):
            last = None
            last_version = -1

            for entry in sorted(file_entries):
                if entry[1] == last_version:
                    print last
                    print entry

                last = entry
                last_version = entry[1]

    for file in file_replicas:
        files_max_version = {}    
        for line in file_replicas[file]:
            '''print line''' 
            node_line = line.strip().split('\t')
            valid_increasing_versions = should_have_increasing_versions(node_line)
            '''print "\n valid_increading_versions =" + str(valid_increasing_versions)'''    
            unique_file_versions = unique_file_version_pair(node_line)
            '''print "\n unique_file_version_pair(node_line) "=  + str(unique_file_versions)'''
            if valid_increasing_versions is False:
                print 'Oops your file versions are inconsistent.'
                break
	    if unique_file_versions is False:
                print 'Oops your files are inconsistent.'
                break
    ''' Each version should be found on at least enough nodes to satisfy W votes.'''
    '''Checking if each version was present on at least w number of quorum sites.
       Except for version 0'''

    for file in file_replicas:
        '''Getting the node name from the log file object'''
        node_file_name_split = file.name.strip().split('/')
        node_name_split = node_file_name_split[node_file_name_split.__len__()-1].split('.')
        ''' For Windows replacing the underscore back by the colon, to get the votes
                for that peer / node '''
        node_name = node_name_split[0].replace("_",":")
        for line in file_replicas[file]:
            node_line = line.strip().split('\t')
            '''If a write operation, then add the vote into the existing vote '''
            if node_line[3] is "W":
            	vote = peer_votes[node_name]
            	files[node_line[0]].addQuromVote(node_line[1], int(vote),)

    '''Now validate if the votes collect are at least w. '''
    for app_file in files:
        if files[app_file].isValidQuorum(int(w)) is False:
            print app_file + " was not written with at least " + str(w) + " quorum votes.";
     